<?php

class GoogleAnalyticsExtension extends DataExtension {

	private static $db = array(
		'GoogleAnalyticsCode' => 'Varchar'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.GoogleAnalytics', TextField::create("GoogleAnalyticsCode")->setTitle(_t('GoogleConfig.CODE',"Google Analytics Code"))->setRightTitle("(UA-XXXXXX-X)"));
	}

	public function AutoTrackJs() {
		return '/' . MODULE_SS_GOOGLE_ANALYTICS_PATH . '/js/autotrack.js';
	}

}
